package com.sachuk.TestTaskBlog;

import com.sachuk.TestTaskBlog.controllers.MainController;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.core.StringContains.containsString;
import static org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers.authenticated;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@WithUserDetails("admin")
public class MainControllerTest {

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private MainController controller;

    @Test
    public void mainPageTest() throws Exception{
        this.mockMvc.perform(get("/main"))
                .andDo(print())
                .andExpect(authenticated())
                .andExpect(content().string(containsString("main")));
    }

    @Test
    public void detailTest() throws Exception{
        this.mockMvc.perform(get("/detail/2"))
                .andDo(print())
                .andExpect(authenticated())
                .andExpect(content().string(containsString("{\"id\":2,\"title\":\"Getting started with JavaFX\",\"content\":\"Lorem ipsum jckfsdf  fkjjhsbfvu h jc zjdfhc\",\"author\":{\"id\":1,\"username\":\"admin\",\"password\":\"admin\",\"active\":true,\"roles\":[\"ADMIN\"],\"enabled\":true,\"authorities\":[\"ADMIN\"],\"credentialsNonExpired\":true,\"accountNonExpired\":true,\"accountNonLocked\":true}}")));
    }
}

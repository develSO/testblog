package com.sachuk.TestTaskBlog.models;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;



//знаю, костыльный метод для сбора статистики который мне не нравиться
public class Statistic {
    public static HashMap<LocalDate, Integer> getStatistic(List<Article> articles){
        HashMap<LocalDate, Integer> days = new HashMap<LocalDate, Integer>();
        for(int i = 0;i<7;i++){
            days.put(LocalDate.now().minusDays(i), 0);
        }
        for (int i = articles.size()-1; i>=0; i--){
            if(days.keySet().contains(articles.get(i).getCreateDate())){
                int count = days.get(articles.get(i).getCreateDate());
                count++;
                days.put(articles.get(i).getCreateDate(), count);
            }
            else
                break;
        }
        return days;
    }
}

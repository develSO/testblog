package com.sachuk.TestTaskBlog.models;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.util.Date;


@Entity
@Data
public class Article {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Size(max=100, message = "Title is long (max 100 characters)")
    @NotEmpty(message = "Title shouldn't be empty")
    private String title;
    private String content;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id")
    private User author;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate createDate = LocalDate.now();


    public Article() {
    }

    public Article(String title, String content, User user) {
        this.title = title;
        this.content = content;
        this.author = user;
    }
}

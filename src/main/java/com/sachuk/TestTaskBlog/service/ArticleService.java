package com.sachuk.TestTaskBlog.service;

import com.sachuk.TestTaskBlog.models.Article;
import com.sachuk.TestTaskBlog.repositories.ArticleRepository;
import com.sachuk.TestTaskBlog.response.ArticleResponse;
import com.sachuk.TestTaskBlog.utils.MappingUtils;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class ArticleService {
    private ArticleRepository artRepo;
    private MappingUtils utils;

    public ArticleResponse getArticleById(long id){
        Optional<Article> article = this.artRepo.findById(id);
        Article art = article.isPresent()?article.get():null;
        ArticleResponse resp = utils.mapToArticleResponse(art);
        return resp;
    }
}

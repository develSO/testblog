package com.sachuk.TestTaskBlog.controllers;

import com.sachuk.TestTaskBlog.models.Article;
import com.sachuk.TestTaskBlog.models.Role;
import com.sachuk.TestTaskBlog.models.Statistic;
import com.sachuk.TestTaskBlog.models.User;
import com.sachuk.TestTaskBlog.repositories.ArticleRepository;
import com.sachuk.TestTaskBlog.response.ArticleResponse;
import com.sachuk.TestTaskBlog.service.ArticleService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.ImportResource;
import org.springframework.core.env.Environment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Optional;

@RestController
@AllArgsConstructor

public class MainController {
    private ArticleRepository articleRepository;
    private final ArticleService articleService;

    @GetMapping("/")
    public String home(Model model){
            return "home";
    }

    @GetMapping("/main")
    public String mainPage(Model model,
                           @PageableDefault(sort = {"id"}, direction = Sort.Direction.DESC) Pageable pageable){
        Page<Article> page = articleRepository.findAll(pageable);
        model.addAttribute("page", page);

//       return this.articleService.getAllArticles();
        return "main";
    }

    @GetMapping("/admin")
    public String adminPanel(@AuthenticationPrincipal User user,
                             Model model){
        if(user.getRoles().contains(Role.ADMIN)){
            HashMap<LocalDate, Integer> stats = Statistic.getStatistic(articleRepository.findAll());
            System.out.println(stats.toString());
            model.addAttribute("stats", stats);  // {2021-04-12=2, 2021-04-11=0, 2021-04-10=0, 2021-04-09=0, 2021-04-08=0, 2021-04-07=0, 2021-04-06=0}
            return "admin";
        }

        else
            return "redirect:/";
    }

    @GetMapping("/add")
    public String addArticle(Model model){
        model.addAttribute("article", new Article());
        return "add";
    }


    @PostMapping("/add")
    public String publishing (@AuthenticationPrincipal User user,
                              @Valid Article article,
                              BindingResult result
                              ){
        if (result.hasErrors())
            return "add";
        else {
            article.setAuthor(user);
            articleRepository.save(article);
            return "redirect:/";
        }
    }

    @GetMapping("/detail/{id}")
    public ArticleResponse article(@PathVariable(value = "id") long id, Model model){
//        Optional<Article> article = articleRepository.findById(id);
//        ArrayList<Article> res = new ArrayList<>();
//        article.ifPresent(res::add);
//        model.addAttribute("article", res);
        return this.articleService.getArticleById(id);
    }
}

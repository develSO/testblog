package com.sachuk.TestTaskBlog.utils;

import com.sachuk.TestTaskBlog.models.Article;
import com.sachuk.TestTaskBlog.response.ArticleResponse;
import org.springframework.stereotype.Service;

@Service
public class MappingUtils {
    public ArticleResponse mapToArticleResponse(Article art){
        ArticleResponse resp = new ArticleResponse();
        resp.setId(art.getId());
        resp.setTitle(art.getTitle());
        resp.setContent(art.getContent());
        resp.setAuthor(art.getAuthor());
        return resp;
    }

    public Article mapToArticle(ArticleResponse resp){
        Article art = new Article();
        art.setAuthor(resp.getAuthor());
        art.setContent(resp.getContent());
        art.setId(resp.getId());
        art.setTitle(resp.getTitle());
        return art;
    }
}

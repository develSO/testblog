package com.sachuk.TestTaskBlog.response;

import lombok.Data;

@Data
public class UserResponse {
    Long id;
    String username;
    boolean active;
}

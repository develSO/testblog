package com.sachuk.TestTaskBlog.response;


import com.sachuk.TestTaskBlog.models.User;
import lombok.Data;

@Data
public class ArticleResponse {
    Long id;
    String title;
    String content;
    User author;
}

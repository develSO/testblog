package com.sachuk.TestTaskBlog;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestTaskBlogApplication {

    public static void main(String[] args) {
        SpringApplication.run(TestTaskBlogApplication.class, args);
    }

}